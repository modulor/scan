<?php 

class Usuarios_model extends CI_Model
{

    public function nuevo($form)
    {
        // guardar en "contacto" 

        $data_c = array(
            'tipo_contacto_id' => $form['rol_id'],
            'nombre' => $form['nombre'],
            'apellido_paterno' => $form['apellido_paterno'],
            'apellido_materno' => $form['apellido_materno'],
            'correo_electronico' => $form['correo_electronico'],
            'telefono' => $form['telefono'],
            'telefono_celular' => $form['telefono_celular'],
            'fecha_creacion' => date('Y-m-d H:i:s'),
            'usuario_creacion' => $this->session->userdata('usuario_uid'),
            'telefono_consultorio' => $form['telefono_consultorio'],
            'nombre_asistente' => $form['nombre_asistente'],
            'descuento' => $form['descuento'],
            'especialidad_medica_uid' => $form['especialidad_medica_uid'],
            'municipio' => $form['municipio'],
            'estado' => $form['estado'],
            'pais' => $form['pais']
        );

        $save = $this->db->insert('contacto', $data_c);

        $contacto_id = $this->db->insert_id();    

        // guardar en "medico_herramienta_sw" si es "medico"

        if($form['rol_id'] == 'med'){
            if(isset($form['herramienta_sw_uid']))
                $herr = $this->guardar_herramientas($contacto_id, $form['herramienta_sw_uid']);

            if(isset($form['tratamiento_uid']))
                $trat = $this->guardar_tratamientos($contacto_id, $form['tratamiento_uid']);
        }

        // guardar en "usuarios"

        $data_u = array(
            'usuario' => $form['usuario'],
            'contrasena' => md5(sha1($form['contrasena'])),
            'rol_id' => $form['rol_id'],
            'fecha_creacion' => date('Y-m-d H:i:s'),
            'contacto_id' => $contacto_id,
        );

        $save = $this->db->insert('usuario', $data_u);

        return true;
    }


    public function lista()
    {
        return $query = $this->db->select("u.usuario, concat(c.nombre,' ',c.apellido_paterno,' ',c.apellido_materno) as nombre_completo, r.descripcion as rol", FALSE)
        ->from('usuario u')
        ->join('contacto c','c.contacto_id = u.contacto_id')
        ->join('rol r','r.rol_id = u.rol_id')
        ->get()
        ->result();
    }


    public function guardar_herramientas($contacto_id, $herramienta_sw_uid)
    {
        foreach($herramienta_sw_uid as $key => $value){
            $data = array(
                'herramienta_sw_uid' => $value,
                'contacto_id' => $contacto_id
            );

            $save = $this->db->insert('medico_herramienta_sw', $data);
        }

        return true;
    }


    public function guardar_tratamientos($contacto_id, $tratamiento_uid)
    {
        foreach($tratamiento_uid as $key => $value){
            $data = array(
                'tratamiento_uid' => $value,
                'contacto_id' => $contacto_id
            );

            $save = $this->db->insert('medico_tratamiento', $data);
        }

        return true;
    }


    public function existe_usuario($usuario)
    {
        return $query = $this->db->select("usuario")
        ->where("usuario",$usuario)
        ->get("usuario")
        ->row();
    }

}

?>