<?php 

class Tipo_estudio_model extends CI_Model
{

    public function lista()
    {
        return $query = $this->db->order_by('descripcion')
        ->get('tipo_estudio')
        ->result();
    }


    public function info($tipo_estudio_uid)
    {
        return $query = $this->db->where('tipo_estudio_uid', $tipo_estudio_uid)
        ->get('tipo_estudio')
        ->row();
    }

}

?>