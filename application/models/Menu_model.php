<?php 

class Menu_model extends CI_Model
{

    public function getMenu($rol_id)
    {
        return $query = $this->db->select('m.menu_display_name')
        ->from('menu m')
        ->join('rol_menu rm', 'rm.menu_uid = m.menu_uid')
        ->where('rm.rol_id', $rol_id)
        ->get()
        ->result();
    }

}

?>