<?php 

/**
* 
*/
class Pacientes_model extends CI_Model
{
    
    public function nuevo_desde_estudio($form)
    {
        $data_c = array(
            'tipo_contacto_id' => 'pac',
            'nombre' => $form['nombre'],
            'apellido_paterno' => $form['apellido_paterno'],
            'apellido_materno' => $form['apellido_materno'],
            'correo_electronico' => $form['correo_electronico'],
            'telefono' => $form['telefono'],
            'telefono_celular' => $form['telefono_celular'],
            'fecha_creacion' => date('Y-m-d H:i:s'),
            'usuario_creacion' => $this->session->userdata('usuario_uid'),
            'municipio' => $form['municipio'],
            'estado' => $form['estado'],
            'pais' => $form['pais']
        );

        $save = $this->db->insert('contacto', $data_c);

        return $this->db->insert_id();    
    }    

}

?>