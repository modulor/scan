<?php 

class Especialidad_medica_model extends CI_Model
{

    public function lista()
    {
        return $query = $this->db->order_by('descripcion')
        ->get('especialidad_medica')
        ->result();
    }

}

?>