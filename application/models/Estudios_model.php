<?php 

class Estudios_model extends CI_Model
{

    public function nuevo($form)
    {
        // obtener costo del estudio
        
        $this->load->model('Tipo_estudio_model');

        $tipo_estudio = $this->Tipo_estudio_model->info($form['tipo_estudio_uid']);

        $precio_estandar = $tipo_estudio->precio_estandar;

        // guardar un usuario si "paciente_id" es igual a 0

        $paciente_id = $form['paciente_id'];

        if($form['paciente_id']==0){
            $this->load->model('Pacientes_model');
            $paciente_id = $this->Pacientes_model->nuevo_desde_estudio($form);
        }

        $data = array(
            'tipo_estudio_uid' => $form['tipo_estudio_uid'],
            'paciente_id' => $paciente_id,
            'medico_id' => $this->session->userdata('usuario_uid'),
            'fecha_creacion'  => date('Y-m-d H:i:s'),
            'costo' => $precio_estandar,
            'usuario_creacion' => $this->session->userdata('usuario_uid')
        );

        $save = $this->db->insert('estudio', $data);

        return $this->db->insert_id();
    }


    public function actualizar_archivo($estudio_uid, $archivo)
    {
        $data['ruta_archivo'] = $archivo;

        $query = $this->db->where('estudio_uid', $estudio_uid)
        ->update('estudio', $data);

        return true;
    }


    public function lista()
    {
        return $query = $this->db->select("e.ruta_archivo, e.costo, concat(c.nombre,' ',c.apellido_paterno,' ',c.apellido_materno) as nombre_paciente, te.descripcion as tipo_estudio", FALSE)
        ->from('estudio e')
        ->join('contacto c', 'c.contacto_id = e.paciente_id')
        ->join('tipo_estudio te', 'te.tipo_estudio_uid = e.tipo_estudio_uid')
        ->get()
        ->result();
    }

}

?>