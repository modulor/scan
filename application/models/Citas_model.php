<?php 

class Citas_model extends CI_Model
{
    public function nuevo($form)
    {
        // guardar un usuario si "paciente_id" es igual a 0

        $paciente_id = $form['paciente_id'];

        if($form['paciente_id']==0){
            $this->load->model('Pacientes_model');
            $paciente_id = $this->Pacientes_model->nuevo_desde_estudio($form);
        }

        // fecha cita

        $fechas = explode(" ", $form['fecha_hora_cita']);

        $fecha_cita = $fechas[0];

        $hora_cita = $fechas[1];

        $data = array(
            'contacto_id' => $this->session->userdata('usuario_uid'),
            'paciente_id' => $paciente_id,
            'fecha_solicitud' => date('Y-m-d H:i:s'),
            'fecha_cita' => $fecha_cita,
            'hora_cita' => $hora_cita,
            'sucursal_id' => $form['sucursal_id']
        );

        $save = $this->db->insert('cita', $data);

        return true;
    }


    public function lista()
    {
        return $query = $this->db->select('c.*, s.nombre_sucursal, concat(con.nombre," ",con.apellido_paterno," ",con.apellido_materno) as nombre_paciente', FALSE)
        ->from('cita c')
        ->join('sucursal s', 's.sucursal_id = c.sucursal_id')
        ->join('contacto con', 'con.contacto_id = c.paciente_id')
        ->get()
        ->result();
    }
}

?>