<?php 

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
    }
    
    public function index()
    {
        $datos_sesion = $this->session->all_userdata();

        if (isset($datos_sesion['login']) && $datos_sesion['login'] == TRUE)
            redirect(base_url('dashboard'), 'refresh');

        if (!isset($_POST['username']))
        {
            $data['contenido_view'] = "login_view";
            $this->load->view('login_view',$data);
        }                   
        else
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required', array('required' => 'You must provide a %s...'));

            if (($this->form_validation->run() == FALSE))
            {
                $data['contenido_view'] = "login_view";
                $this->load->view('login_view', $data);
            }               
            else
            {                
                $existeemail = $this->Login_model->userExist($_POST['username'], $_POST['password']);
                if ($existeemail)
                {
                    $datos_sesion = array(
                        'login' => TRUE,
                        'usuario_uid' => $existeemail->usuario_uid,
                        'username' => $_POST['username'],
                        'level' => $existeemail->rol_id
                    );                                           

                    $this->session->set_userdata($datos_sesion);                      

                    $this->redirect_user($datos_sesion['level']);           
                } 
                else
                {
                    $data['error_login'] = "Usuario y/o Contrasena incorrecto(s)";
                    $this->load->view('login_view', $data);
                }
            }
        }
    }


    public function redirect_user($level)
    {
        switch ($level) {
            case 'admin':
                $datos['menu'] = 'menu/menu_admin_view';
                $controlador = 'dashboard';
            break;
            
            default:
                $datos['menu'] = 'menu/menu_medico_view';
                $controlador = 'dashboard';
            break;
        }

        $this->session->set_userdata($datos);  

        redirect(base_url($controlador));
    }


    public function log_out()
    {
        $this->session->sess_destroy();
        redirect(base_url(),'refresh');
    } 
}

?>