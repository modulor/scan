<?php 

class Estudios extends CI_Controller
{

    function __construct()
    {       
        parent::__construct();

        if(!$this->session->userdata('login'))
            redirect(base_url('login'),"refresh");

        $this->load->model('Estudios_model');
    }


    public function index()
    {
        $data['_css'] = array(
            'plugins/datatables/jquery.dataTables.min.css',
            'plugins/datatables/buttons.bootstrap.min.css'
        );

        $data['_js'] = array(
            'plugins/datatables/jquery.dataTables.min.js',
            'plugins/datatables/dataTables.bootstrap.js',
            'pages/estudios/lista.js'
        );

        $data['estudios'] = $this->Estudios_model->lista();

        $data['contenido_view'] = 'estudios/estudios_lista_view';
        
        $this->load->view('dashboard_view', $data);
    }


    public function nuevo()
    {
        $data['_css'] = array(
            'plugins/datatables/jquery.dataTables.min.css',
            'plugins/datatables/buttons.bootstrap.min.css',
            'plugins/dropzone/dropzone.css',
            'plugins/imgupload/css/fileinput.min.css'
        );

        $data['_js'] = array(
            'plugins/datatables/jquery.dataTables.min.js',
            'plugins/datatables/dataTables.bootstrap.js',
            'plugins/parsleyjs/dist/parsley.min.js',
            'plugins/parsleyjs/src/i18n/es.js',
            'plugins/imgupload/js/fileinput.js',
            'plugins/dropzone/dropzone.js',
            'pages/estudios/nuevo.js'
        );

        if($this->input->post())
        {
            $estudio_uid = $this->Estudios_model->nuevo($this->input->post());

            // subir foto de perfil

            if($_FILES['archivo']['name']!=""){

                // si no existe el directorio lo creamos

                $ruta = 'uploads/archivos/'.date('Y').'/'.date('m');

                if (!is_dir('./'.$ruta)) {
                    mkdir('./'.$ruta, 0755, TRUE);
                }
                
                $config['file_name']            = md5(sha1(date('YmdHis')));
                $config['upload_path']          = $ruta;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2000;
                $config['max_width']            = 2024;
                $config['max_height']           = 1768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('archivo')){
                    $data['error'] = array('error' => $this->upload->display_errors());
                    $archivo = '';
                }
                else{
                    $data_img = $this->upload->data();
                    $archivo = $ruta.'/'.$data_img['file_name'];
                }
            
                $actualizar_foto = $this->Estudios_model->actualizar_archivo($estudio_uid, $archivo);

            }

            if(!isset($data['error']))
                $data['ok'] = true;
        }

        $this->load->model('Tipo_estudio_model');

        $data['tipos_estudios'] = $this->Tipo_estudio_model->lista();

        $this->load->model('Contactos_model');

        $data['pacientes'] = $this->Contactos_model->lista('pac');

        $data['contenido_view'] = 'estudios/estudios_nuevo_view';

        $this->load->view('dashboard_view', $data);
    }

    // para mostrar un nuevo campo de archivo o sección

    public function new_element($type = "", $num = 2)
    {
        $data['num'] = $num;
        $this->load->view('estudios/estudios_new_'.$type.'_view', $data);
    }

}

?>