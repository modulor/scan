<?php 

class Dashboard extends CI_Controller
{
    function __construct()
    {       
        parent::__construct();

        if(!$this->session->userdata('login'))
            redirect(base_url('login'),"refresh");
    }

    public function index()
    {   
        $data['contenido_view'] = 'home/'.strtolower($this->session->userdata('level')).'_home_view';
        $this->load->view("dashboard_view", $data);
    }
}

?>