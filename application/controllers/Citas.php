<?php 

class Citas extends CI_Controller
{

    function __construct()
    {       
        parent::__construct();

        if(!$this->session->userdata('login'))
            redirect(base_url('login'),"refresh");

        $this->load->model('Citas_model');
    }


    public function index()
    {
        $data['_css'] = array(
            'plugins/datatables/jquery.dataTables.min.css',
            'plugins/datatables/buttons.bootstrap.min.css'
        );

        $data['_js'] = array(
            'plugins/datatables/jquery.dataTables.min.js',
            'plugins/datatables/dataTables.bootstrap.js',
            'pages/citas/lista.js'
        );

        $data['citas'] = $this->Citas_model->lista();

        $data['contenido_view'] = 'citas/citas_lista_view';
        
        $this->load->view('dashboard_view', $data);
    }


    public function nuevo()
    {
        $data['_js'] = array(
            'plugins/datatables/jquery.dataTables.min.js',
            'plugins/datatables/dataTables.bootstrap.js',
            'plugins/parsleyjs/dist/parsley.min.js',
            'plugins/parsleyjs/src/i18n/es.js',
            'plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.es.js',
            'pages/citas/nuevo.js'
        );

        $data['_css'] = array(
            'plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
        );

        if($this->input->post())
        {
            $data['ok'] = $this->Citas_model->nuevo($this->input->post());
        }

        // lista de sucursales

        $this->load->model('Sucursales_model');

        $data['sucursales'] = $this->Sucursales_model->lista();

        $this->load->model('Contactos_model');

        $data['pacientes'] = $this->Contactos_model->lista('pac');

        $data['contenido_view'] = 'citas/citas_nuevo_view';

        $this->load->view('dashboard_view', $data);
    }

}

?>