<?php 

class Usuarios extends CI_Controller
{

    function __construct()
    {       
        parent::__construct();

        if(!$this->session->userdata('login'))
            redirect(base_url('login'),"refresh");

        $this->load->model('Roles_model');

        $this->load->model('Usuarios_model');
    }


    public function index()
    {
        $data['_css'] = array(
            'plugins/datatables/jquery.dataTables.min.css',
            'plugins/datatables/buttons.bootstrap.min.css'
        );

        $data['_js'] = array(
            'plugins/datatables/jquery.dataTables.min.js',
            'plugins/datatables/dataTables.bootstrap.js',
            'pages/usuarios/lista.js'
        );

        $data['usuarios'] = $this->Usuarios_model->lista();

        $data['contenido_view'] = 'usuarios/usuarios_lista_view';
        
        $this->load->view('dashboard_view', $data);
    }


    public function nuevo()
    {
        $data['_css'] = array(
            'plugins/select2/select2.css',
            'plugins/select2/select2-bootstrap.css'
        );

        $data['_js'] = array(
            'plugins/parsleyjs/dist/parsley.min.js',
            'plugins/parsleyjs/src/i18n/es.js',
            'plugins/select2/select2.min.js',
            'pages/usuarios/nuevo.js'
        );

        if($this->input->post())
        {
            $save = $this->Usuarios_model->nuevo($this->input->post());
            $data['ok'] = true;
        }

        // lista de roles

        $data['roles'] = $this->Roles_model->lista();

        $this->load->model('Especialidad_medica_model');

        // lista de especialidades

        $data['especialidades'] = $this->Especialidad_medica_model->lista();

        // heramientas

        $this->load->model('Herramientas_sw_model');

        $data['herramientas'] = $this->Herramientas_sw_model->lista();

        // tratamientos

        $this->load->model('Tratamientos_model');

        $data['tratamientos'] = $this->Tratamientos_model->lista();

        $data['contenido_view'] = 'usuarios/usuarios_nuevo_view';

        $this->load->view('dashboard_view', $data);
    }


    public function info_ajax()
    {
        $json['default'] = false;
        if($this->input->post())
        {
            $this->load->model('Contactos_model');
            $json['contacto'] = $this->Contactos_model->info($_POST['contacto_id']);
        }

        echo json_encode($json);
    }

    // validar usuario no exista previamente

    public function validar_usuario()
    {
        if($_GET['usuario'] != $_GET['usuario_old']){
            $buscar = $this->Usuarios_model->existe_usuario($_GET['usuario']);
            if(count($buscar)>0)
                echo "existe";
            else
                echo "no";
        }
        else
            echo "no";
    }

}

?>