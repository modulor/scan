<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.ico') ?>">

        <title>Scan3D</title>

        <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/core.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/icons.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/components.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/pages.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/menu.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/responsive.css') ?>" rel="stylesheet" type="text/css">

        <?php
            if(isset($_css)):
                foreach ($_css as $css) {
        ?>
        <link rel="stylesheet" href="<?php echo base_url('assets/'.$css) ?>">
        <?php
                }
            endif;
        ?>

        <link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet" type="text/css">

        <script src="<?php echo base_url('assets/js/modernizr.min.js') ?>"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        
    </head>


    <body class="fixed-left">
        
        <!-- Begin page -->
        <div id="wrapper">
        
            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- logo -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="<?php echo base_url('dashboard') ?>" class="logo text-white"><i class="fa fa-user-md"></i> <span>Scan3D</span> </a>
                    </div>
                </div>

                <!-- Navbar -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <ul class="nav navbar-nav navbar-right pull-right">                                
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown" aria-expanded="true"><img src="<?php echo base_url('assets/images/users/avatar-1.jpg') ?>" alt="user-img" class="img-circle"> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url('dashboard') ?>"><i class="ti-user m-r-5"></i> Perfil</a></li>
                                        <li><a href="<?php echo base_url('dashboard') ?>"><i class="ti-settings m-r-5"></i> Configuraci&oacute;n</a></li>
                                        <li><a href="<?php echo base_url('login/log_out') ?>"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <div id="sidebar-menu">
                        <ul>
                            <li class="text-muted menu-title">Menu</li>
                            <?php $this->load->view("menu/menu_view") ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>                
            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <?php $this->load->view($contenido_view) ?>
                    </div>
                    <!-- end container -->

                </div>
                <!-- end content -->

                <!-- FOOTER -->
                <footer class="footer text-right">
                    <?php echo date("Y") ?> &copy; Scan3D.
                </footer>
                <!-- End FOOTER -->

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        <script>
            var resizefunc = [];
            var base_url = '<?php echo base_url() ?>';
        </script>

        <!-- jQuery  -->

        <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/detect.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/fastclick.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.slimscroll.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.blockUI.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/waves.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/wow.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.nicescroll.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.scrollTo.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.core.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.app.js') ?>"></script>
        <?php 
            if(isset($_js)):
                foreach($_js as $js):
        ?>
        <script src="<?php echo base_url('assets/'.$js) ?>"></script>
        <?php
                endforeach;
            endif;
        ?>
    </body>
</html>