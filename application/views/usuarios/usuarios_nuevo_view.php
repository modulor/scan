<!-- page title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h4 class="page-title">Usuarios <small>Nuevo</small></h4>            
        </div>
    </div>
</div>
<!-- page title -->

<?php 
    if(isset($ok)):
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong><i class="fa fa-check"></i></strong> La informaci&oacute;n se ha guardado correctamente.
        </div>
    </div>
</div>
<?php 
    endif;
?>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="m-t-0 header-title"><b>Nuevo usuario</b></h4>
            <p class="text-muted m-b-30 font-13">
                * campos obligatorios
            </p>
            <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('usuarios/nuevo') ?>" enctype="multipart/form-data" accept-charset="utf-8" data-parsley-validate novalidate>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="rol_id">*Rol</label>
                    <div class="col-md-10">
                        <select name="rol_id" id="rol_id" class="form-control" parsley-trigger="change" required>
                            <option value=""></option>
                            <?php 
                                foreach($roles as $rol):
                            ?>
                            <option value="<?php echo $rol->rol_id ?>"><?php echo $rol->descripcion ?></option>
                            <?php
                                endforeach;
                            ?>
                        </select>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="usuario">*Usuario</label>
                    <div class="col-md-10">
                        <input type="text" name="usuario" class="form-control" id="usuario" parsley-trigger="change" required>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="contrasena">*Contrase&ntilde;a</label>
                    <div class="col-md-10">
                        <input type="password" name="contrasena" class="form-control" id="contrasena" parsley-trigger="change" required data-parsley-equalto="#contrasena2">
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="contrasena2">*Confirmar contrase&ntilde;a</label>
                    <div class="col-md-10">
                        <input type="password" name="contrasena2" class="form-control" id="contrasena2" parsley-trigger="change" required data-parsley-equalto="#contrasena">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="nombre">*Nombre</label>
                    <div class="col-md-10">
                        <input type="text" name="nombre" class="form-control" id="nombre" parsley-trigger="change" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="apellido_paterno">*Apellido Paterno</label>
                    <div class="col-md-10">
                        <input type="text" name="apellido_paterno" class="form-control" id="apellido_paterno" parsley-trigger="change" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="apellido_materno">Apellido Materno</label>
                    <div class="col-md-10">
                        <input type="text" name="apellido_materno" class="form-control" id="apellido_materno" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="correo_electronico">Correo Electr&oacute;nico</label>
                    <div class="col-md-10">
                        <input type="text" name="correo_electronico" class="form-control" id="correo_electronico" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="telefono">Tel&eacute;fono</label>
                    <div class="col-md-10">
                        <input type="text" name="telefono" class="form-control" id="telefono" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="telefono_celular">Tel&eacute;fono celular</label>
                    <div class="col-md-10">
                        <input type="text" name="telefono_celular" class="form-control" id="telefono_celular" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="direccion">Direcci&oacute;n</label>
                    <div class="col-md-10">
                        <textarea name="direccion" id="direccion" class="form-control" rows="5"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="codigo_postal">C&oacute;digo postal</label>
                    <div class="col-md-10">
                        <input type="text" name="codigo_postal" class="form-control" id="codigo_postal" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="municipio">Municipio</label>
                    <div class="col-md-10">
                        <input type="text" name="municipio" class="form-control" id="municipio" parsley-trigger="change">
                    </div>
                </div>                
                <div class="form-group">
                    <label class="col-md-2 control-label" for="estado">Estado</label>
                    <div class="col-md-10">
                        <input type="text" name="estado" class="form-control" id="estado" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="pais">Pa&iacute;s</label>
                    <div class="col-md-10">
                        <input type="text" name="pais" class="form-control" id="pais" parsley-trigger="change">
                    </div>
                </div>
                <div id="data_med" class="hide">
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="descuento">Descuento</label>
                        <div class="col-md-10">
                            <input type="number" name="descuento" class="form-control" id="descuento" parsley-trigger="change">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="telefono_consultorio">Tel&eacute;fono consultorio</label>
                        <div class="col-md-10">
                            <input type="text" name="telefono_consultorio" class="form-control" id="telefono_consultorio" parsley-trigger="change">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="nombre_asistente">Nombre asistente</label>
                        <div class="col-md-10">
                            <input type="text" name="nombre_asistente" class="form-control" id="nombre_asistente" parsley-trigger="change">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="especialidad_medica_uid">*Especialidad m&eacute;dica</label>
                        <div class="col-md-10">
                            <select name="especialidad_medica_uid" id="especialidad_medica_uid" class="form-control" parsley-trigger="change" required>
                                <option value=""></option>
                                <?php 
                                    foreach($especialidades as $esp):
                                ?>
                                <option value="<?php echo $esp->especialidad_medica_uid ?>"><?php echo $esp->descripcion ?></option>
                                <?php
                                    endforeach;
                                ?>
                            </select>
                        </div>                    
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="herramienta_sw_uid">Herramienta(s)</label>
                        <div class="col-md-6">
                            <select name="herramienta_sw_uid[]" id="herramienta_sw_uid" class="form-contrl sel-mul" multiple="multiple">
                                <?php 
                                    foreach ($herramientas as $row):
                                ?>
                                <option value="<?php echo $row->herramienta_sw_uid ?>"><?php echo $row->descripcion ?></option>                        
                                <?php
                                    endforeach;
                                ?>
                            </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="tratamiento_uid">Tratamiento(s)</label>
                        <div class="col-md-6">
                            <select name="tratamiento_uid[]" id="tratamiento_uid" class="form-contrl sel-mul" multiple="multiple">
                                <?php 
                                    foreach ($tratamientos as $row):
                                ?>
                                <option value="<?php echo $row->tratamiento_uid ?>"><?php echo $row->descripcion ?></option>                        
                                <?php
                                    endforeach;
                                ?>
                            </select>
                        </div>
                    </div>               
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-2">
                        <button id="btnSubmit" type="submit" class="btn btn-primary btn-lg">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal error -->
<div class="modal modal-danger fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="modalErrorLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-danger" id="modalErrorLabel"><i class="fa fa-warning"></i></h4>
            </div>
            <div class="modal-body text-center"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div> 