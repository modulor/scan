<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.ico') ?>">
        <title>Scan3D - Login</title>
        <?php $this->load->view("_css_view") ?>
        <link href="<?php echo base_url('assets/css/login.css') ?>" rel="stylesheet" type="text/css">        
    </head>
    <body>

        <div class="wrapper-page">

            <div class="text-center">
                <a href="<?php echo base_url() ?>" class="logo logo-lg">
                    <i class="fa fa-user-md"></i> <span>Scan3D</span> 
                </a>
            </div>

            <form class="form-horizontal m-t-20" action="<?php echo base_url('login') ?>" method="post">

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" name="username" type="text" required="" placeholder="Usuario" autofocus="">
                        <i class="md md-account-circle form-control-feedback l-h-34"></i>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" name="password" type="password" required="" placeholder="Contrase&ntilde;a">
                        <i class="md md-vpn-key form-control-feedback l-h-34"></i>
                    </div>
                </div>

                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-custom btn-lg w-md waves-effect waves-light" type="submit">Entrar</button>
                    </div>
                </div>
                
            </form>
        </div>
        
        <script>
            var resizefunc = [];
        </script>

        <?php $this->load->view("_js_view") ?>
    
    </body>
</html>