<!-- page title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h4 class="page-title">Estudios <small>Nuevo</small></h4>            
        </div>
    </div>
</div>
<!-- page title -->

<?php 
    if(isset($ok)):
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong><i class="fa fa-check"></i></strong> La informaci&oacute;n se ha guardado correctamente.
        </div>
    </div>
</div>
<?php 
    endif;
?>

<?php 
    if(isset($error)):
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong><i class="fa fa-check"></i></strong> <?php print_r($error) ?>
        </div>
    </div>
</div>
<?php 
    endif;
?>

<form role="form" method="post" action="<?php echo base_url('estudios/nuevo') ?>" enctype="multipart/form-data" accept-charset="utf-8" data-parsley-validate novalidate>
    <div class="row">
        <div class="col-sm-6">
            <div class="card-box">
                <p class="text-muted m-b-30 font-13">
                    * campos obligatorios
                </p>
                
                <div class="form-group">
                    <label class="control-label" for="tipo_estudio_uid">*Tipo de estudio</label>
                    <select name="tipo_estudio_uid" id="tipo_estudio_uid" class="form-control" parsley-trigger="change" required>
                        <option value=""></option>
                        <?php 
                            foreach($tipos_estudios as $row):
                        ?>
                        <option value="<?php echo $row->tipo_estudio_uid ?>"><?php echo $row->descripcion ?></option>
                        <?php
                            endforeach;
                        ?>
                    </select>                    
                </div>                
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Paciente</label>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="button" onclick="buscarPaciente();" class="btn btn-success"><i class="fa fa-search"></i> Buscar paciente</button>
                            <input type="hidden" id="paciente_id" name="paciente_id" value="0">    
                        </div>
                    </div>                    
                </div>

                <div class="form-group">
                    <label class="control-label" for="nombre">*Nombre</label>
                    <input type="text" name="nombre" class="form-control" id="nombre" parsley-trigger="change" required>
                </div>
                <div class="form-group">
                    <label class="control-label" for="apellido_paterno">*Apellido Paterno</label>
                    <input type="text" name="apellido_paterno" class="form-control" id="apellido_paterno" parsley-trigger="change" required>
                </div>
                <div class="form-group">
                    <label class="control-label" for="apellido_materno">Apellido Materno</label>
                    <input type="text" name="apellido_materno" class="form-control" id="apellido_materno" parsley-trigger="change">
                </div>
                <div class="form-group">
                    <label class="control-label" for="correo_electronico">Correo Electr&oacute;nico</label>
                    <input type="text" name="correo_electronico" class="form-control" id="correo_electronico" parsley-trigger="change">
                </div>
                <div class="form-group">
                    <label class="control-label" for="telefono">Tel&eacute;fono</label>
                    <input type="text" name="telefono" class="form-control" id="telefono" parsley-trigger="change">
                </div>
                <div class="form-group">
                    <label class="control-label" for="telefono_celular">Tel&eacute;fono celular</label>
                    <input type="text" name="telefono_celular" class="form-control" id="telefono_celular" parsley-trigger="change">
                </div>
                <div class="form-group">
                    <label class="control-label" for="direccion">Direcci&oacute;n</label>
                    <textarea name="direccion" id="direccion" class="form-control" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <label class="control-label" for="codigo_postal">C&oacute;digo postal</label>
                    <input type="text" name="codigo_postal" class="form-control" id="codigo_postal" parsley-trigger="change">
                </div>
                <div class="form-group">
                    <label class="control-label" for="municipio">Municipio</label>
                    <input type="text" name="municipio" class="form-control" id="municipio" parsley-trigger="change">
                </div>                
                <div class="form-group">
                    <label class="control-label" for="estado">Estado</label>
                    <input type="text" name="estado" class="form-control" id="estado" parsley-trigger="change">
                </div>
                <div class="form-group">
                    <label class="control-label" for="pais">Pa&iacute;s</label>
                    <input type="text" name="pais" class="form-control" id="pais" parsley-trigger="change">
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card-box">
                <div class="form-group">
                    <label class="control-label" for="seccion_nombre">*Secci&oacute;n</label>
                    <select name="seccion_nombre" id="seccion_nombre" class="form-control" parsley-trigger="change" required>
                        <option value=""></option>
                        <option value="Rx 3D">Rx 3D</option>
                        <option value="Rx 2D">Rx 2D</option>
                        <option value="Fotografia Intraoral">Fotografia Intraoral</option>
                        <option value="Fotografia Extraoral">Fotografia Extraoral</option>
                        <option value="Modelos Digitales">Modelos Digitales</option>
                        <option value="Reporte">Reporte</option>
                    </select>                    
                </div>
                <div class="form-group">
                    <div class="dZUpload dropzone">
                          <div class="dz-default dz-message">Adjuntar archivos</div>
                    </div>
                </div>
            </div>
            <div id="newSections"></div>
            <div id="newSectionsLoading"></div>
            <div class="text-center">
                <input type="hidden" id="numSections" value="1">
                <button onclick="Javascript:newElement('section');" type="button" class="btn btn-success"><i class="fa fa-plus"></i> Agregar secci&oacute;n</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <button id="btnSubmit" type="button" class="btn btn-primary btn-lg">Guardar</button>
        </div>
    </div> 
</form>

<div class="modal modal-default fade" id="modalBuscarPacientes" tabindex="-1" role="dialog" aria-labelledby="modalBuscarPacientesLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalBuscarPacientesLabel"><i class="fa fa-search"></i> Buscar paciente</h4>
            </div>
            <div class="modal-body">
                <div class="card-box table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Paciente</th>
                                <th>Acci&oacute;n</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                foreach($pacientes as $row):
                            ?>
                            <tr>
                                <td><?php echo $row->nombre." ".$row->apellido_paterno." ".$row->apellido_materno ?></td>
                                <td>
                                    <button name="selBuscPac" onclick="seleccionarPaciente(<?php echo $row->contacto_id ?>);" type="button" class="btn btn-success" title="Agregar"><i class="fa fa-check"></i> Seleccionar</button>
                                </td>
                            </tr>
                            <?php 
                                endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div> 