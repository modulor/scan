<div class="card-box">
    <div class="form-group">
        <label class="control-label" for="seccion_nombre">*Secci&oacute;n</label>
        <select name="seccion_nombre" id="seccion_nombre" class="form-control" parsley-trigger="change" required>
            <option value=""></option>
            <option value="Rx 3D">Rx 3D</option>
            <option value="Rx 2D">Rx 2D</option>
            <option value="Fotografia Intraoral">Fotografia Intraoral</option>
            <option value="Fotografia Extraoral">Fotografia Extraoral</option>
            <option value="Modelos Digitales">Modelos Digitales</option>
            <option value="Reporte">Reporte</option>
        </select>                    
    </div>
    <div class="form-group">
        <div id="dZUpload_<?php echo $num ?>" class="dZUpload dropzone">
              <div class="dz-default dz-message">Adjuntar archivos</div>
        </div>
    </div>
</div>