<!-- page title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <h4 class="page-title">Citas <small>Nueva</small></h4>            
        </div>
    </div>
</div>
<!-- page title -->

<?php 
    if(isset($ok)):
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong><i class="fa fa-check"></i></strong> La informaci&oacute;n se ha guardado correctamente.
        </div>
    </div>
</div>
<?php 
    endif;
?>

<?php 
    if(isset($error)):
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong><i class="fa fa-check"></i></strong> <?php print_r($error) ?>
        </div>
    </div>
</div>
<?php 
    endif;
?>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="m-t-0 header-title"><b>Nueva cita</b></h4>
            <p class="text-muted m-b-30 font-13">
                * campos obligatorios
            </p>
            <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('citas/nuevo') ?>" enctype="multipart/form-data" accept-charset="utf-8" data-parsley-validate novalidate>
                <div class="form-group">
                    <div class="col-md-2 control-label"><label for="fecha_hora_cita">*Fecha y hora</label></div>
                    <div class="col-md-10">
                        <input type="text" name="fecha_hora_cita" id="fecha_hora_cita" class="form-control datetimepicker" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2 control-label"><label>Paciente</label></div>
                    <div class="col-md-10">
                        <button type="button" onclick="buscarPaciente();" class="btn btn-success"><i class="fa fa-search"></i> Buscar paciente</button>
                        <input type="hidden" id="paciente_id" name="paciente_id" value="0">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" for="nombre">*Nombre</label>
                    <div class="col-md-10">
                        <input type="text" name="nombre" class="form-control" id="nombre" parsley-trigger="change" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="apellido_paterno">*Apellido Paterno</label>
                    <div class="col-md-10">
                        <input type="text" name="apellido_paterno" class="form-control" id="apellido_paterno" parsley-trigger="change" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="apellido_materno">Apellido Materno</label>
                    <div class="col-md-10">
                        <input type="text" name="apellido_materno" class="form-control" id="apellido_materno" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="correo_electronico">Correo Electr&oacute;nico</label>
                    <div class="col-md-10">
                        <input type="text" name="correo_electronico" class="form-control" id="correo_electronico" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="telefono">Tel&eacute;fono</label>
                    <div class="col-md-10">
                        <input type="text" name="telefono" class="form-control" id="telefono" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="telefono_celular">Tel&eacute;fono celular</label>
                    <div class="col-md-10">
                        <input type="text" name="telefono_celular" class="form-control" id="telefono_celular" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="direccion">Direcci&oacute;n</label>
                    <div class="col-md-10">
                        <textarea name="direccion" id="direccion" class="form-control" rows="5"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="codigo_postal">C&oacute;digo postal</label>
                    <div class="col-md-10">
                        <input type="text" name="codigo_postal" class="form-control" id="codigo_postal" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="municipio">Municipio</label>
                    <div class="col-md-10">
                        <input type="text" name="municipio" class="form-control" id="municipio" parsley-trigger="change">
                    </div>
                </div>                
                <div class="form-group">
                    <label class="col-md-2 control-label" for="estado">Estado</label>
                    <div class="col-md-10">
                        <input type="text" name="estado" class="form-control" id="estado" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="pais">Pa&iacute;s</label>
                    <div class="col-md-10">
                        <input type="text" name="pais" class="form-control" id="pais" parsley-trigger="change">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-md-2 control-label" for="sucursal_id">Sucursal</label>
                    <div class="col-md-10">
                        <select name="sucursal_id" id="sucursal_id" class="form-control">
                            <option value=""></option>
                            <?php 
                                foreach ($sucursales as $row):
                            ?>
                            <option value="<?php echo $row->sucursal_id ?>"><?php echo $row->nombre_sucursal ?></option>
                            <?php
                                endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-2">
                        <button id="btnSubmit" type="submit" class="btn btn-primary btn-lg">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> 

<div class="modal modal-default fade" id="modalBuscarPacientes" tabindex="-1" role="dialog" aria-labelledby="modalBuscarPacientesLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalBuscarPacientesLabel"><i class="fa fa-search"></i> Buscar paciente</h4>
            </div>
            <div class="modal-body">
                <div class="card-box table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Paciente</th>
                                <th>Acci&oacute;n</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                foreach($pacientes as $row):
                            ?>
                            <tr>
                                <td><?php echo $row->nombre." ".$row->apellido_paterno." ".$row->apellido_materno ?></td>
                                <td>
                                    <button name="selBuscPac" onclick="seleccionarPaciente(<?php echo $row->contacto_id ?>);" type="button" class="btn btn-success" title="Agregar"><i class="fa fa-check"></i> Seleccionar</button>
                                </td>
                            </tr>
                            <?php 
                                endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div> 