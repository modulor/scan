<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="pull-right">
                <a href="<?php echo base_url('citas/nuevo') ?>" class="btn btn-success"><i class="fa fa-plus"></i> Nueva cita</a>
            </div>
            <h4 class="page-title">Citas <small>Lista</small></h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Paciente</th>
                        <th>Fecha</th>
                        <th>Hora</th>
                        <th>Sucursal</th>
                        <th>Acci&oacute;n</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach($citas as $row):
                    ?>
                    <tr>
                        <td><?php echo $row->nombre_paciente ?></td>
                        <td><?php echo $row->fecha_cita ?></td>
                        <td><?php echo $row->hora_cita ?></td>
                        <td><?php echo $row->nombre_sucursal ?></td>
                        <td>
                            <button type="button" class="btn btn-success" title="Ver"><i class="fa fa-info-circle"></i></button>
                            <button type="button" class="btn btn-primary" title="Editar"><i class="fa fa-pencil"></i></button>
                            <button type="button" class="btn btn-danger" title="Borrar"><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                    <?php 
                        endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>