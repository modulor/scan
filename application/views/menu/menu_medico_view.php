<li class="has-submenu">
    <a href="<?php echo base_url('dashboard') ?>"><i class="md md-dashboard"></i>Dashboard</a>
</li>

<li class="has_sub">
    <a href="javascript:void(0);" class="waves-effect waves-primary"><i class="md md-assignment"></i> <span>Citas</span>
     <span class="menu-arrow"></span></a>
    <ul class="list-unstyled">
        <li><a href="<?php echo base_url('citas/nuevo') ?>">Nuevo</a></li>
        <li><a href="<?php echo base_url('citas/') ?>">Lista de citas</a></li>                                    
    </ul>
</li>

<li class="has_sub">
    <a href="javascript:void(0);" class="waves-effect waves-primary"><i class="fa fa-users"></i> <span>Estudios</span>
     <span class="menu-arrow"></span></a>
    <ul class="list-unstyled">
        <li><a href="<?php echo base_url('estudios/nuevo') ?>">Nuevo</a></li>
        <li><a href="<?php echo base_url('estudios/') ?>">Lista de estudios</a></li>                                    
    </ul>
</li>