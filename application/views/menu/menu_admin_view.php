<li>
    <a href="<?php echo base_url('dashboard') ?>" class="waves-effect waves-primary">
        <i class="md md-dashboard"></i><span>Dashboard</span>
    </a>
</li>

<li class="has_sub">
    <a href="javascript:void(0);" class="waves-effect waves-primary"><i class="fa fa-users"></i> <span>Usuarios</span>
     <span class="menu-arrow"></span></a>
    <ul class="list-unstyled">
        <li><a href="<?php echo base_url('usuarios/nuevo') ?>">Agregar usuario</a></li>
        <li><a href="<?php echo base_url('usuarios/') ?>">Lista de usuarios</a></li>                                    
    </ul>
</li>