<li>
    <a href="<?php echo base_url('dashboard') ?>" class="waves-effect waves-primary">
        <i class="fa fa-angle-right"></i><span>Dashboard</span>
    </a>
</li>

<?php 
    $menu = $this->Menu_model->getMenu($this->session->userdata('level'));
    foreach($menu as $menu):
?>
<li>
    <a href="<?php echo base_url(strtolower($menu->menu_display_name)) ?>" class="waves-effect waves-primary">
        <i class="fa fa-angle-right"></i><span><?php echo $menu->menu_display_name ?></span>
    </a>
</li>
<?php
    endforeach;
?>