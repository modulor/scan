<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Bienvenido</h4>
    </div>
</div>
<!-- Page-Title -->

<div class="row">
    <div class="col-lg-4 col-lg-offset-4">
        <div class="card-box text-center">          
            <a href="<?php echo base_url('video/ver/'.$this->session->userdata('entry_id')) ?>" class="btn btn-default btn-lg"><i class="fa fa-play"></i> Ver Curso</a>            
        </div>
    </div>
</div>