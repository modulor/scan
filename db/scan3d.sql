-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-04-2017 a las 23:13:17
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `scan3d`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo`
--

CREATE TABLE `archivo` (
  `archivo_uid` int(11) NOT NULL,
  `estudio_seccion_uid` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `ruta_archivo` varchar(255) DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_ultima_modificacion` datetime NOT NULL,
  `usuario_creacion` varchar(255) NOT NULL,
  `usuario_ultima_modificacion` varchar(255) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE `cita` (
  `cita_uid` int(11) NOT NULL,
  `contacto_id` int(11) NOT NULL,
  `paciente_id` int(11) NOT NULL,
  `fecha_solicitud` datetime NOT NULL,
  `fecha_cita` date NOT NULL,
  `hora_cita` time NOT NULL,
  `sucursal_id` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_ultima_modificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_creacion` varchar(255) NOT NULL,
  `usuario_ultima_modificacion` varchar(255) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cita`
--

INSERT INTO `cita` (`cita_uid`, `contacto_id`, `paciente_id`, `fecha_solicitud`, `fecha_cita`, `hora_cita`, `sucursal_id`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 10, 14, '2017-04-02 02:05:45', '2017-04-01', '14:00:12', 2, '2017-04-01 18:05:45', '2017-04-01 18:05:45', '', '', 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE `comentario` (
  `comentario_uid` int(11) NOT NULL,
  `archivo_uid` int(11) NOT NULL,
  `comentario` varchar(1000) NOT NULL,
  `contacto_id` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_ultima_modificacion` datetime NOT NULL,
  `usuario_creacion` varchar(255) NOT NULL,
  `usuario_ultima_modificacion` varchar(255) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compania`
--

CREATE TABLE `compania` (
  `compania_id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_ultima_modificacion` datetime NOT NULL,
  `usuario_creacion` int(11) NOT NULL,
  `usuario_ultima_modificacion` int(11) NOT NULL,
  `eliminado` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compania`
--

INSERT INTO `compania` (`compania_id`, `nombre`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 'DEFAULT', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `contacto_id` int(11) NOT NULL,
  `tipo_contacto_id` varchar(40) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido_paterno` varchar(255) NOT NULL,
  `apellido_materno` varchar(255) NOT NULL,
  `correo_electronico` varchar(320) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `telefono_celular` varchar(255) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_ultima_modificacion` datetime NOT NULL,
  `usuario_creacion` int(11) NOT NULL,
  `usuario_ultima_modificacion` int(11) NOT NULL,
  `eliminado` varchar(1) NOT NULL DEFAULT 'N',
  `nombre_clinica` varchar(125) DEFAULT NULL,
  `descuento` decimal(10,0) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `codigo_postal` varchar(5) DEFAULT NULL,
  `municipio` varchar(100) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `pais` varchar(50) DEFAULT NULL,
  `telefono_consultorio` varchar(125) DEFAULT NULL,
  `nombre_asistente` varchar(125) DEFAULT NULL,
  `especialidad_medica_uid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`contacto_id`, `tipo_contacto_id`, `nombre`, `apellido_paterno`, `apellido_materno`, `correo_electronico`, `telefono`, `telefono_celular`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`, `nombre_clinica`, `descuento`, `direccion`, `codigo_postal`, `municipio`, `estado`, `pais`, `telefono_consultorio`, `nombre_asistente`, `especialidad_medica_uid`) VALUES
(13, 'med', 'Juan', 'Gomez', '', '', '', '', '2017-03-26 10:57:55', '0000-00-00 00:00:00', 1, 0, 'N', NULL, '0', NULL, NULL, '', '', '', '', '', 7),
(14, 'pac', 'Mario', 'Lopez', '', '', '', '', '2017-03-31 19:32:39', '0000-00-00 00:00:00', 10, 0, 'N', NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especialidad_medica`
--

CREATE TABLE `especialidad_medica` (
  `especialidad_medica_uid` int(11) NOT NULL,
  `descripcion` varchar(125) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_ultima_modificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_creacion` varchar(255) NOT NULL,
  `usuario_ultima_modificacion` varchar(255) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `especialidad_medica`
--

INSERT INTO `especialidad_medica` (`especialidad_medica_uid`, `descripcion`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 'Dentista General', '2017-03-25 17:10:58', '2017-03-25 17:10:58', 'root@localhost', 'root@localhost', 'N'),
(2, 'Ortodoncista', '2017-03-25 17:10:58', '2017-03-25 17:10:58', 'root@localhost', 'root@localhost', 'N'),
(3, 'Periodoncista', '2017-03-25 17:10:58', '2017-03-25 17:10:58', 'root@localhost', 'root@localhost', 'N'),
(4, 'Prostondoncista', '2017-03-25 17:10:58', '2017-03-25 17:10:58', 'root@localhost', 'root@localhost', 'N'),
(5, 'Implantologo', '2017-03-25 17:10:58', '2017-03-25 17:10:58', 'root@localhost', 'root@localhost', 'N'),
(6, 'Endodoncista', '2017-03-25 17:10:58', '2017-03-25 17:10:58', 'root@localhost', 'root@localhost', 'N'),
(7, 'Cirujano Maxilofacial', '2017-03-25 17:10:58', '2017-03-25 17:10:58', 'root@localhost', 'root@localhost', 'N'),
(8, 'Odontopediatra', '2017-03-25 17:10:58', '2017-03-25 17:10:58', 'root@localhost', 'root@localhost', 'N'),
(9, 'Otorrinolaringologo', '2017-03-25 17:10:58', '2017-03-25 17:10:58', 'root@localhost', 'root@localhost', 'N'),
(10, 'Cirujano Plastico', '2017-03-25 17:10:58', '2017-03-25 17:10:58', 'root@localhost', 'root@localhost', 'N'),
(11, 'Traumatologo', '2017-03-25 17:10:58', '2017-03-25 17:10:58', 'root@localhost', 'root@localhost', 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudio`
--

CREATE TABLE `estudio` (
  `estudio_uid` int(11) NOT NULL,
  `medico_id` int(11) NOT NULL,
  `paciente_id` int(11) NOT NULL,
  `tipo_estudio_uid` int(11) NOT NULL,
  `fecha_estudio` datetime NOT NULL,
  `costo` decimal(10,0) NOT NULL,
  `precio_real` decimal(10,0) NOT NULL,
  `descuento` decimal(10,0) DEFAULT NULL,
  `estatus` varchar(40) DEFAULT NULL,
  `ruta_archivo` varchar(255) DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_ultima_modificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_creacion` varchar(255) NOT NULL,
  `usuario_ultima_modificacion` varchar(255) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudio`
--

INSERT INTO `estudio` (`estudio_uid`, `medico_id`, `paciente_id`, `tipo_estudio_uid`, `fecha_estudio`, `costo`, `precio_real`, `descuento`, `estatus`, `ruta_archivo`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 10, 14, 3, '0000-00-00 00:00:00', '230', '0', NULL, NULL, 'uploads/archivos/2017/03/1643afe286236a8c995d32ffe451d37a.png', '2017-03-31 19:32:39', '2017-03-31 11:32:39', '10', '', 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudio_seccion`
--

CREATE TABLE `estudio_seccion` (
  `estudio_seccion_uid` int(11) NOT NULL,
  `estudio_uid` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_ultima_modificacion` datetime NOT NULL,
  `usuario_creacion` varchar(255) NOT NULL,
  `usuario_ultima_modificacion` varchar(255) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `herramienta_sw`
--

CREATE TABLE `herramienta_sw` (
  `herramienta_sw_uid` int(11) NOT NULL,
  `descripcion` varchar(125) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_ultima_modificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_creacion` varchar(255) NOT NULL,
  `usuario_ultima_modificacion` varchar(255) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `herramienta_sw`
--

INSERT INTO `herramienta_sw` (`herramienta_sw_uid`, `descripcion`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 'I-CAT Vision', '2017-03-26 00:52:25', '2017-03-26 00:52:25', 'root@localhost', 'root@localhost', 'N'),
(2, 'Tx Studio / Anatomage', '2017-03-26 00:52:25', '2017-03-26 00:52:25', 'root@localhost', 'root@localhost', 'N'),
(3, 'Blue Sky Bio', '2017-03-26 00:52:25', '2017-03-26 00:52:25', 'root@localhost', 'root@localhost', 'N'),
(4, 'Planmeca Romexis', '2017-03-26 00:52:25', '2017-03-26 00:52:25', 'root@localhost', 'root@localhost', 'N'),
(5, 'Muelas del juicio', '2017-03-26 00:52:25', '2017-03-26 00:52:25', 'root@localhost', 'root@localhost', 'N'),
(6, 'Morita Romexis', '2017-03-26 00:52:25', '2017-03-26 00:52:25', 'root@localhost', 'root@localhost', 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medico_herramienta_sw`
--

CREATE TABLE `medico_herramienta_sw` (
  `medico_herramienta_sw_uid` int(11) NOT NULL,
  `contacto_id` int(11) NOT NULL,
  `herramienta_sw_uid` int(11) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medico_tratamiento`
--

CREATE TABLE `medico_tratamiento` (
  `medico_tratamiento_uid` int(11) NOT NULL,
  `contacto_id` int(11) NOT NULL,
  `tratamiento_uid` int(11) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `menu_uid` int(11) NOT NULL,
  `menu_display_name` varchar(255) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_ultima_modificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_creacion` varchar(255) NOT NULL,
  `usuario_ultima_modificacion` varchar(255) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`menu_uid`, `menu_display_name`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 'Usuarios', '2017-04-07 12:54:17', '2017-04-07 12:54:17', 'root@localhost', 'root@localhost', 'N'),
(2, 'Pacientes', '2017-04-07 12:54:17', '2017-04-07 12:54:17', 'root@localhost', 'root@localhost', 'N'),
(3, 'Estudios', '2017-04-07 12:54:17', '2017-04-07 12:54:17', 'root@localhost', 'root@localhost', 'N'),
(4, 'Citas', '2017-04-07 12:54:17', '2017-04-07 12:54:17', 'root@localhost', 'root@localhost', 'N'),
(5, 'Reportes', '2017-04-07 12:54:17', '2017-04-07 12:54:17', 'root@localhost', 'root@localhost', 'N'),
(6, 'Contenido', '2017-04-07 12:54:17', '2017-04-07 12:54:17', 'root@localhost', 'root@localhost', 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `rol_uid` int(11) NOT NULL,
  `rol_id` varchar(40) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_ultima_modificacion` datetime NOT NULL,
  `usuario_creacion` int(11) NOT NULL,
  `usuario_ultima_modificacion` int(11) NOT NULL,
  `eliminado` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`rol_uid`, `rol_id`, `descripcion`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 'admin', 'ADMINISTRADOR', '2017-03-24 01:00:00', '0000-00-00 00:00:00', 0, 0, 'N'),
(2, 'med', 'MEDICO', '2017-03-25 00:00:00', '0000-00-00 00:00:00', 0, 0, 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_menu`
--

CREATE TABLE `rol_menu` (
  `rol_menu_uid` int(11) NOT NULL,
  `rol_id` varchar(40) NOT NULL,
  `menu_uid` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_ultima_modificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_creacion` varchar(255) NOT NULL,
  `usuario_ultima_modificacion` varchar(255) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol_menu`
--

INSERT INTO `rol_menu` (`rol_menu_uid`, `rol_id`, `menu_uid`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 'admin', 1, '2017-04-07 12:54:17', '2017-04-07 12:54:17', 'root@localhost', 'root@localhost', 'N'),
(2, 'admin', 2, '2017-04-07 12:54:17', '2017-04-07 12:54:17', 'root@localhost', 'root@localhost', 'N'),
(3, 'admin', 3, '2017-04-07 12:54:17', '2017-04-07 12:54:17', 'root@localhost', 'root@localhost', 'N'),
(4, 'admin', 4, '2017-04-07 12:54:17', '2017-04-07 12:54:17', 'root@localhost', 'root@localhost', 'N'),
(5, 'admin', 5, '2017-04-07 12:54:17', '2017-04-07 12:54:17', 'root@localhost', 'root@localhost', 'N'),
(6, 'admin', 6, '2017-04-07 12:54:17', '2017-04-07 12:54:17', 'root@localhost', 'root@localhost', 'N'),
(8, 'med', 3, '2017-04-07 13:26:06', '2017-04-07 13:26:06', '', '', 'N'),
(9, 'med', 4, '2017-04-07 13:26:06', '2017-04-07 13:26:06', '', '', 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `sucursal_id` int(11) NOT NULL,
  `compania_id` int(11) NOT NULL,
  `nombre_sucursal` varchar(255) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_ultima_modificacion` datetime NOT NULL,
  `usuario_creacion` int(11) NOT NULL,
  `usuario_ultima_modificacion` int(11) NOT NULL,
  `eliminado` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`sucursal_id`, `compania_id`, `nombre_sucursal`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 1, 'SUCURSAL A', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 'N'),
(2, 1, 'SUCURSAL B', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_contacto`
--

CREATE TABLE `tipo_contacto` (
  `tipo_contacto_uid` int(11) NOT NULL,
  `tipo_contacto_id` varchar(40) NOT NULL,
  `descripcion` varchar(128) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_ultima_modificacion` datetime NOT NULL,
  `usuario_creacion` int(11) NOT NULL,
  `usuario_ultima_modificacion` int(11) NOT NULL,
  `eliminado` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_contacto`
--

INSERT INTO `tipo_contacto` (`tipo_contacto_uid`, `tipo_contacto_id`, `descripcion`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 'default', 'CONTACTO POR DEFAULT', '2017-03-24 00:00:00', '0000-00-00 00:00:00', 0, 0, 'N'),
(2, 'pac', 'PACIENTE', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 'N'),
(3, 'med', 'MEDICO', '2017-03-25 00:00:00', '0000-00-00 00:00:00', 0, 0, 'N'),
(4, 'emp', 'EMPLEADO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_estudio`
--

CREATE TABLE `tipo_estudio` (
  `tipo_estudio_uid` int(11) NOT NULL,
  `descripcion` varchar(125) NOT NULL,
  `extension` varchar(50) DEFAULT NULL,
  `precio_estandar` decimal(10,0) NOT NULL DEFAULT '0',
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_ultima_modificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_creacion` varchar(255) NOT NULL,
  `usuario_ultima_modificacion` varchar(255) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_estudio`
--

INSERT INTO `tipo_estudio` (`tipo_estudio_uid`, `descripcion`, `extension`, `precio_estandar`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 'ConeBeam Completo', 'dcm', '120', '2017-03-25 19:10:16', '2017-03-25 19:10:16', 'root@localhost', 'root@localhost', 'N'),
(2, 'ConeBeam Macizo Facial', 'dcm', '190', '2017-03-25 19:10:16', '2017-03-25 19:10:16', 'root@localhost', 'root@localhost', 'N'),
(3, 'ConeBeam HD', 'dcm', '230', '2017-03-25 19:10:16', '2017-03-25 19:10:16', 'root@localhost', 'root@localhost', 'N'),
(4, 'Panoramica', 'jpg', '90', '2017-03-25 19:10:16', '2017-03-25 19:10:16', 'root@localhost', 'root@localhost', 'N'),
(5, 'Ceph', 'jpg', '300', '2017-03-25 19:10:16', '2017-03-25 19:10:16', 'root@localhost', 'root@localhost', 'N'),
(6, 'Fotografia clinica', 'jpg', '45', '2017-03-25 19:10:16', '2017-03-25 19:10:16', 'root@localhost', 'root@localhost', 'N'),
(7, 'Modelos digitales', 'stl', '900', '2017-03-25 19:10:16', '2017-03-25 19:10:16', 'root@localhost', 'root@localhost', 'N'),
(8, 'Modelos de yeso', NULL, '120', '2017-03-25 19:10:16', '2017-03-25 19:10:16', 'root@localhost', 'root@localhost', 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tratamiento`
--

CREATE TABLE `tratamiento` (
  `tratamiento_uid` int(11) NOT NULL,
  `descripcion` varchar(125) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_ultima_modificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_creacion` varchar(255) NOT NULL,
  `usuario_ultima_modificacion` varchar(255) NOT NULL,
  `eliminado` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tratamiento`
--

INSERT INTO `tratamiento` (`tratamiento_uid`, `descripcion`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 'Cirugia de implantes', '2017-03-26 01:30:00', '2017-03-26 01:30:00', 'root@localhost', 'root@localhost', 'N'),
(2, 'Rehabilitacion de implantes', '2017-03-26 01:30:00', '2017-03-26 01:30:00', 'root@localhost', 'root@localhost', 'N'),
(3, 'Periodontopatias', '2017-03-26 01:30:00', '2017-03-26 01:30:00', 'root@localhost', 'root@localhost', 'N'),
(4, 'Cirugía ortognatica', '2017-03-26 01:30:00', '2017-03-26 01:30:00', 'root@localhost', 'root@localhost', 'N'),
(5, 'Muelas del juicio', '2017-03-26 01:30:00', '2017-03-26 01:30:00', 'root@localhost', 'root@localhost', 'N'),
(6, 'Endodoncias', '2017-03-26 01:30:00', '2017-03-26 01:30:00', 'root@localhost', 'root@localhost', 'N'),
(7, 'Odontopediatria', '2017-03-26 01:30:00', '2017-03-26 01:30:00', 'root@localhost', 'root@localhost', 'N'),
(8, 'Apnea del sueño', '2017-03-26 01:30:00', '2017-03-26 01:30:00', 'root@localhost', 'root@localhost', 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `usuario_uid` int(11) NOT NULL,
  `usuario` varchar(40) NOT NULL,
  `contrasena` varchar(32) NOT NULL,
  `rol_id` varchar(40) NOT NULL,
  `contacto_id` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_ultima_modificacion` datetime NOT NULL,
  `usuario_creacion` int(11) NOT NULL,
  `usuario_ultima_modificacion` int(11) NOT NULL,
  `eliminado` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usuario_uid`, `usuario`, `contrasena`, `rol_id`, `contacto_id`, `fecha_creacion`, `fecha_ultima_modificacion`, `usuario_creacion`, `usuario_ultima_modificacion`, `eliminado`) VALUES
(1, 'admin', '8a8c371d507232a432bd30e18bd3ba7b', 'admin', 1, '2017-03-24 00:00:00', '0000-00-00 00:00:00', 0, 0, 'N'),
(10, 'medico1', '8a8c371d507232a432bd30e18bd3ba7b', 'med', 13, '2017-03-26 10:57:55', '0000-00-00 00:00:00', 0, 0, 'N');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `archivo`
--
ALTER TABLE `archivo`
  ADD PRIMARY KEY (`archivo_uid`),
  ADD KEY `fk_archivo_estudio_seccion` (`estudio_seccion_uid`);

--
-- Indices de la tabla `cita`
--
ALTER TABLE `cita`
  ADD PRIMARY KEY (`cita_uid`);

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`comentario_uid`),
  ADD KEY `fk_comentario_archivo` (`archivo_uid`),
  ADD KEY `fk_comentario_contacto` (`contacto_id`);

--
-- Indices de la tabla `compania`
--
ALTER TABLE `compania`
  ADD PRIMARY KEY (`compania_id`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`contacto_id`);

--
-- Indices de la tabla `especialidad_medica`
--
ALTER TABLE `especialidad_medica`
  ADD PRIMARY KEY (`especialidad_medica_uid`);

--
-- Indices de la tabla `estudio`
--
ALTER TABLE `estudio`
  ADD PRIMARY KEY (`estudio_uid`),
  ADD KEY `fk_estudio_tipo_estudio` (`tipo_estudio_uid`);

--
-- Indices de la tabla `estudio_seccion`
--
ALTER TABLE `estudio_seccion`
  ADD PRIMARY KEY (`estudio_seccion_uid`),
  ADD KEY `fk_estudio_seccion_estudio` (`estudio_uid`);

--
-- Indices de la tabla `herramienta_sw`
--
ALTER TABLE `herramienta_sw`
  ADD PRIMARY KEY (`herramienta_sw_uid`);

--
-- Indices de la tabla `medico_herramienta_sw`
--
ALTER TABLE `medico_herramienta_sw`
  ADD PRIMARY KEY (`medico_herramienta_sw_uid`),
  ADD UNIQUE KEY `ak_medico_herramienta_sw_contacto_id_herramienta_sw_uid` (`contacto_id`,`herramienta_sw_uid`),
  ADD KEY `fk_medico_herramienta_sw_herramienta_sw` (`herramienta_sw_uid`);

--
-- Indices de la tabla `medico_tratamiento`
--
ALTER TABLE `medico_tratamiento`
  ADD PRIMARY KEY (`medico_tratamiento_uid`),
  ADD UNIQUE KEY `ak_medico_tratamiento_contacto_id_tratamiento_uid` (`contacto_id`,`tratamiento_uid`),
  ADD KEY `fk_medico_tratamiento_tratamiento` (`tratamiento_uid`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_uid`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`rol_uid`);

--
-- Indices de la tabla `rol_menu`
--
ALTER TABLE `rol_menu`
  ADD PRIMARY KEY (`rol_menu_uid`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`sucursal_id`);

--
-- Indices de la tabla `tipo_contacto`
--
ALTER TABLE `tipo_contacto`
  ADD PRIMARY KEY (`tipo_contacto_uid`);

--
-- Indices de la tabla `tipo_estudio`
--
ALTER TABLE `tipo_estudio`
  ADD PRIMARY KEY (`tipo_estudio_uid`);

--
-- Indices de la tabla `tratamiento`
--
ALTER TABLE `tratamiento`
  ADD PRIMARY KEY (`tratamiento_uid`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuario_uid`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `archivo`
--
ALTER TABLE `archivo`
  MODIFY `archivo_uid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cita`
--
ALTER TABLE `cita`
  MODIFY `cita_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
  MODIFY `comentario_uid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `compania`
--
ALTER TABLE `compania`
  MODIFY `compania_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `contacto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `especialidad_medica`
--
ALTER TABLE `especialidad_medica`
  MODIFY `especialidad_medica_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `estudio`
--
ALTER TABLE `estudio`
  MODIFY `estudio_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `estudio_seccion`
--
ALTER TABLE `estudio_seccion`
  MODIFY `estudio_seccion_uid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `herramienta_sw`
--
ALTER TABLE `herramienta_sw`
  MODIFY `herramienta_sw_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `medico_herramienta_sw`
--
ALTER TABLE `medico_herramienta_sw`
  MODIFY `medico_herramienta_sw_uid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `medico_tratamiento`
--
ALTER TABLE `medico_tratamiento`
  MODIFY `medico_tratamiento_uid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `rol_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `rol_menu`
--
ALTER TABLE `rol_menu`
  MODIFY `rol_menu_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  MODIFY `sucursal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipo_contacto`
--
ALTER TABLE `tipo_contacto`
  MODIFY `tipo_contacto_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tipo_estudio`
--
ALTER TABLE `tipo_estudio`
  MODIFY `tipo_estudio_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `tratamiento`
--
ALTER TABLE `tratamiento`
  MODIFY `tratamiento_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuario_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `archivo`
--
ALTER TABLE `archivo`
  ADD CONSTRAINT `fk_archivo_estudio_seccion` FOREIGN KEY (`estudio_seccion_uid`) REFERENCES `estudio_seccion` (`estudio_seccion_uid`);

--
-- Filtros para la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD CONSTRAINT `fk_comentario_archivo` FOREIGN KEY (`archivo_uid`) REFERENCES `archivo` (`archivo_uid`),
  ADD CONSTRAINT `fk_comentario_contacto` FOREIGN KEY (`contacto_id`) REFERENCES `contacto` (`contacto_id`);

--
-- Filtros para la tabla `estudio`
--
ALTER TABLE `estudio`
  ADD CONSTRAINT `fk_estudio_tipo_estudio` FOREIGN KEY (`tipo_estudio_uid`) REFERENCES `tipo_estudio` (`tipo_estudio_uid`);

--
-- Filtros para la tabla `estudio_seccion`
--
ALTER TABLE `estudio_seccion`
  ADD CONSTRAINT `fk_estudio_seccion_estudio` FOREIGN KEY (`estudio_uid`) REFERENCES `estudio` (`estudio_uid`);

--
-- Filtros para la tabla `medico_herramienta_sw`
--
ALTER TABLE `medico_herramienta_sw`
  ADD CONSTRAINT `fk_medico_herramienta_sw_contacto` FOREIGN KEY (`contacto_id`) REFERENCES `contacto` (`contacto_id`),
  ADD CONSTRAINT `fk_medico_herramienta_sw_herramienta_sw` FOREIGN KEY (`herramienta_sw_uid`) REFERENCES `herramienta_sw` (`herramienta_sw_uid`);

--
-- Filtros para la tabla `medico_tratamiento`
--
ALTER TABLE `medico_tratamiento`
  ADD CONSTRAINT `fk_medico_tratamiento_contacto` FOREIGN KEY (`contacto_id`) REFERENCES `contacto` (`contacto_id`),
  ADD CONSTRAINT `fk_medico_tratamiento_tratamiento` FOREIGN KEY (`tratamiento_uid`) REFERENCES `tratamiento` (`tratamiento_uid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
