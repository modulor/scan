$(function(){

    // validar form

    $('form').parsley();

    // para la tabla de usuarios en el modal

    $('table').dataTable({
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    // dropzone - files

    Dropzone.autoDiscover = false;

    $(".dZUpload").dropzone({
        url: "hn_SimpeFileUploader.ashx",
        addRemoveLinks: true,
        success: function (file, response) {
            var imgName = response;
            file.previewElement.classList.add("dz-success");
            console.log("Successfully uploaded :" + imgName);
        },
        error: function (file, response) {
            file.previewElement.classList.add("dz-error");
        }
    });

});


function buscarPaciente()
{
    $("#modalBuscarPacientes").modal('show');
}

function seleccionarPaciente(contacto_id)
{
    $.ajax({
        url: base_url+"usuarios/info_ajax/",
        type: "POST",
        data: {
            contacto_id: contacto_id
        },
        dataType: 'json',
        beforeSend: function(){

            $("button[name='selBuscPac']").addClass('disabled');

        },
        success: function(res){

            $("#paciente_id").val(res.contacto.contacto_id);
            $("#nombre").val(res.contacto.nombre);
            $("#apellido_paterno").val(res.contacto.apellido_paterno);
            $("#apellido_materno").val(res.contacto.apellido_materno);
            $("#correo_electronico").val(res.contacto.correo_electronico);
            $("#telefono").val(res.contacto.telefono);
            $("#telefono_celular").val(res.contacto.telefono_celular);
            $("#direccion").val(res.contacto.direccion);
            $("#codigo_postal").val(res.contacto.codigo_postal);
            $("#municipio").val(res.contacto.municipio);
            $("#estado").val(res.contacto.estado);
            $("#pais").val(res.contacto.pais);

            $("#modalBuscarPacientes").modal('hide');

            $("button[name='selBuscPac']").removeClass('disabled');
        },
        error: function (res){
            alert('error');
            console.log(res);
        }
    });
}


// nuevo archivo || seccion

function newElement(type) {

    var elemento, elementoLoading;

    var num = parseInt($("#numSections").val())+1;

    $("#numSections").val(num);

    if(type == "file")
    {
        elemento = "newFiles";
        elementoLoading = "newFilesLoading";
    }
    else
    {
        elemento = "newSections";
        elementoLoading = "newSectionsLoading";
    }

    $.ajax({
        url: base_url+"estudios/new_element/"+type+"/"+num,
        type: "GET",
        dataType: 'html',
        beforeSend: function(){
            $("#"+elementoLoading).html('<div class="text-center"><i class="fa fa-gear fa-spin fa-3x"></i></div>');
        },
        success: function(res){
            $("#"+elementoLoading).html('');
            $("#"+elemento).append(res);
            $("#dZUpload_"+num).dropzone({
                url: "hn_SimpeFileUploader.ashx",
                addRemoveLinks: true,
                success: function (file, response) {
                    var imgName = response;
                    file.previewElement.classList.add("dz-success");
                    console.log("Successfully uploaded :" + imgName);
                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                }
            });
        },
        error: function (res){
            alert('error');
            console.log(res);
        }
    });
}