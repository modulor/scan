$(function(){

    // tabla de usuarios

    $('table').dataTable({
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });


    

});


// ver informacion del estudiante

// function verEstudiante(users_id)
// {
//     $('#modalInfo').modal('show');

//     $.ajax({
//         url: base_url+"estudiantes/ver",
//         type: "POST",
//         data: {
//             users_id: users_id
//         },
//         dataType: 'html',
//         beforeSend: function(){
//             $("#modalInfo .modal-body").html('<div class="text-center"><i class="fa fa-gear fa-spin fa-3x"></i></div>');
//         },
//         success: function(res){
//             $("#modalInfo .modal-body").html(res);
//         },
//         error: function (res){
//             alert('error...');
//             console.log(res);
//         }
//     });
// }