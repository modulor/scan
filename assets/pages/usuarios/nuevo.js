$(function(){

    // validar form

    $('form').parsley();

    // mostrar datos para un tipo de usuario 

    $("#rol_id").change(function(){
        $("#data_med").addClass('hide');
        $("#especialidad_medica_uid").removeAttr("required");
        if(this.value=='med')
        {
            $("#especialidad_medica_uid").attr("required","");
            $("#data_med").removeClass('hide');
        }
    });

    // select2 - multiple

    $(".sel-mul").select2();

    // validar si existe un "usuario" previamente

    $('#usuario').focusin(function () {

        $(this).off().focusout(function () {
            
            var usuario = $(this).val();
            var usuario_old = '';

            if (typeof $("#usuario_old").val() != 'undefined')
                usuario_old = $("#usuario_old").val();

            if(usuario != ""){
                $.get(base_url+'/usuarios/validar_usuario/', {'usuario': usuario, 'usuario_old': usuario_old}).done(function (response) {
                    
                    if (response == 'existe') 
                    {   
                        $('#modalError .modal-body').html('El usuario <strong>"'+usuario+'"</strong> ya se encuentra registrado.<br>Por favor elija otro nombre de usuario');
                        $('#modalError').modal();    

                        // dehabilitar el submit
                        $('#btnSubmit').prop('disabled',true);
                    }
                    else
                        $('#btnSubmit').prop('disabled',false);

                });            
            }
        });
    });

});